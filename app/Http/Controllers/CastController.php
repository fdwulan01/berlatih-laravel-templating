<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('AdminLTE.Casts.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success','Cast Berhasil Disimpan!');
    }

    public function index(){
        $casts = DB::table('casts')->get();
        //dd($casts);
        return view('AdminLTE.Casts.index', compact('casts'));
    }

    public function show($id){
        $cast = DB::table('casts')->where('id', $id)->first(); //first() dipake karna biar dapet datanya satu doang
        //dd($cast);
        return view('AdminLTE.Casts.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('casts')->where('id', $id)->first(); //first() dipake karna biar dapet datanya satu doang

        return view('AdminLTE.Casts.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);

        return redirect('/cast')->with('success','Berhasil Update Cast!');
    }

    public function destroy($id){
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast')->with('success','Cast Berhasil Dihapus!');
    }
}
