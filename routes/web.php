<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@form');
//Route::get('/halo', 'FormController@greet');
Route::post('/welcome', 'AuthController@greet');
Route::get('/master', function(){
    return view('AdminLTE.master');
});
Route::get('/table', function(){
    return view('AdminLTE.Contents.table');
});
Route::get('/data-table', function(){
    return view('AdminLTE.Contents.data-table');
});

Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{id}', 'CastController@destroy');