@extends('AdminLTE.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Casts Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success')}}
            </div>
        @endif
        <a class="btn btn-primary mb-2" href="/cast/create">Create New Cast</a>
        <table class="table table-bordered">
            <thead>
                <tr align="center">
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($casts as $key => $cast)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $cast->nama }}</td>
                        <td>{{ $cast->umur }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td style="display: flex">
                            <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/cast/{{$cast->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" align="center">No Casts</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection