@extends('AdminLTE.master')

@section('header')
<h1>Data Cast {{ $cast->id }}</h1>
@endsection

@section('content')
    <div class="mt-3 ml-3">
        <h5>Nama : {{ $cast->nama }}</h5>
        <h5>Umur : {{ $cast->umur }}</h5>
        <h5>Bio : {{ $cast->bio }}</h5>
    </div>
@endsection