@extends('AdminLTE.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Cast {{ $cast->id }}</h3>
    </div>
              <!-- /.card-header -->
              <!-- form start -->
    <form role="form", action="/cast/{{ $cast->id }}", method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}" placeholder="Masukan nama">
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="integer" class="form-control" id="umur" name="umur" value="{{ old('nama', $cast->umur) }}" placeholder="Masukan umur">
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('nama', $cast->bio) }}" placeholder="Masukan bio">
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
                <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
@endsection